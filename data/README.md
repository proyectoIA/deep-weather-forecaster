# Verificación de comportamiento ante pruebas externas

Para probar el funcionamiento de los modelos sobre otro contexto de datos se intentó buscar datasets que tengan la mayor cantidad de datos comunes respecto a los utilizados para el entrenamiento de los modelos.

A pesar que no se encontraron la totalidad de las columnas, se encontró un dataset dentro del contexto colombiano proveniente de Instituto de Hidrología, Meteorología y Estudios Ambientales [IDEAM](http://www.pronosticosyalertas.gov.co/datos-abiertos-ideam). El dataset tomado es un dataset de pronóstico del tiempo de la colección de datos abiertos de este instituto.

Este dataset contenía el registro de datos atmosféricos tomados cada hora en diferentes localidades del país. Este consta de x columnas entre las que se describen las columnas de interés para el estudio:

- **Fecha:** corresponde a la fecha en la que fue tomada la medición atmosférica.
- **Hora:** corresponde a la hora en la que fue tomada la medición atmosférica.
- **Temperatura:** corresponde a la temperatura medida en grados centígrados.
- **Velocidad del Viento:** corresponde a la magnitud de la velocidad del viento medida en [m/s]
- **Dirección del Viento:** corresponde a la medición de la dirección del viento en grados.
- **Presión:** corresponde a la presión atmosférica medida en [mbar].
- **Punto de Rocío:** corresponde a la temperatura atmosférica relativa a la humedad en el aire, esta es medida en grados centígrados.
- **Humedad:** corresponde a la humedad relativa la cual mide el porcentaje de vapor de agua presente en el aire.


