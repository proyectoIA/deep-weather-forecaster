# Deep Weather Forecaster

![](imgs/banner.png)

[![Open In Collab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/gist/bemc22/83bcb0eec664ee691a4450731ab85c70/deep_weather_forecaster.ipynb)

Los seres humanos a lo largo de la historia han tenido un especial interés en el pronóstico de ciertos factores relacionados con el estado del tiempo, normalmente para la predicción de ciertas variables atmosféricas como la temperatura, presión atmosférica, vientos, humedades o precipitaciones. Si bien la necesidad de la determinación del estado del clima radica desde las antiguas civilizaciones como los babilonios o el imperio chino, hoy en día las técnicas y modelos predictivos han mejorado enormemente respecto a los métodos arcaicos.
 
El objetivo de este proyecto consiste en determinar **¿cómo se podría comportar el clima en días posteriores base a recolecciones de datos pasados?** Por lo cual se buscará implementar un modelo de aprendizaje de máquina (basado en modelos de redes neuronales) que utilizando un ventaneo de cierto número de días para variables atmosféricas intente predecir una variable en los próximos días.
 
La importancia de los modelos de predicciones climáticas radica en que estos son vitales para actividades de gestión y toma de decisiones en áreas de monitoreo de recursos naturales, predicción de desastres naturales, protección de salud humana, desarrollo de actividades turísticas y de movilidad, entre otros.



[Codigo](https://colab.research.google.com/gist/bemc22/83bcb0eec664ee691a4450731ab85c70/deep_weather_forecaster.ipynb) | [Diapositivas](imgs/diapositivas.mp4) | [Video](https://www.youtube.com/watch?v=mA6vfxTUKo0)
